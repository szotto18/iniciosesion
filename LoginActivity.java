package com.szystems.www.loginumg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText txtemail, txtpassword;
    private Spinner sptipo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtemail = (EditText) findViewById(R.id.txtemail);
        txtpassword = (EditText) findViewById(R.id.txtpassword);
        sptipo = (Spinner) findViewById(R.id.sptipo);
    }



    public void login(View view)
    {
        String user = txtemail.getText().toString();
        String pass = txtpassword.getText().toString();
        String selec = sptipo.getSelectedItem().toString();
        String profe = "Profesor", est = "Estudiante";


        if (user.endsWith("gt") && pass.equals("123"))
        {
            if (selec.equals(est))
            {
                Intent i = new Intent(this, StudentActivity.class);
                startActivity(i);
            }
            if (selec.equals(profe))
            {
                Intent i = new Intent(this, TeacherActivity.class);
                startActivity(i);
            }
        }
        else
        {

            Toast notification = Toast.makeText(this, "Credenciales Invalidas ", Toast.LENGTH_SHORT);
            notification.show();
        }
    }
}
