package com.szystems.www.loginumg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class StudentActivity extends AppCompatActivity {

    private Spinner spactivo;
    private ListView lvl;
    private List<String> listado;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        listado= new ArrayList<>();

        spactivo= (Spinner) findViewById(R.id.spactivo);
        lvl= (ListView) findViewById(R.id.lvl);

        adapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listado );
        lvl.setAdapter(adapter);

    }

    public void add(View view){
        String value= spactivo.getSelectedItem().toString();
        listado.add(value);
        adapter.notifyDataSetChanged();
    }
}
